//Javascript Synchronous vs. Asynchronous

//Synchronous Javascript
//Javascript is by default is synchronous meaning that only one statement run/execute at a time

/*
console.log("Hello World");

console.log("Goodbye");

for(let i = 0; i <= 100; i++){
	console.log(i)
}

//blocking, is just a slow process of code.
console.log("Hello World")

//Asynchronous Javascript
//not occuring at the same time

function printMe() {
	console.log('print me')
};

//printMe();
function test() {
	console.log('test')
};

setTimeout(printMe, 5000); //delaying the printOut
test();
*/

//The Fetch API allows you to asynchronously request for a resource (data)
//the fetch receives a PROMISE
//A 'Promise' is an object that represents the eventual completion (or failure) of an asynchronous function and its resulting value

//fetch() is a method in JS, which allows us to send a request to an API and process its response

//fetch(url, {options}).then(response => response.json()).then(data => {console.log(data)})

//url = > the url to resource/routes from the Server
//optional objects = > it contains additional information about our requests such as method, the bosy and the headers. 

//it parse the response as JSON
//process the results



//A promise may be in one of 3 possible states: fulfilled, rejected or pending


//Retrieves all post (GET)
//No.3 & 4
fetch('https://jsonplaceholder.typicode.com/todos',{
	method:'GET'
})

.then(response => response.json())
.then(data => {
	console.log(data)
})


//No.5 & 6

fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method:'GET'
})

.then(response => response.json())
.then(data => {
	console.log(data)
})


//No.7

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type':'application/json'
	},
	body: JSON.stringify({
		completed: false,
		id: 201,
		title: 'Created To Do List Item',
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json))


//No.8

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type':'application/json'
	},
	body: JSON.stringify({
		description: 'To update the my to do list with a different data structure',
		id:1,
		status:"Pending",
		title: "Updated To Do List Item",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json))

//No.9 & 10
fetch('https://jsonplaceholder.typicode.com/todos', {
	method:'POST',
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		completed: true,
		dateCompleted: "new date",
		id:201,
		status:"new status",
		title: "new title",
		userId:1
	})
})
.then(res => res.json())
.then(data => console.log(data))

//No.11
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method:'PATCH',
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		completed: false,
		dateCompleted: "07/09/21",
		id:1,
		status:"Complete",
		title: "delectus aut autem",
		userId:1
	})
})
.then(res => res.json())
.then(data => console.log(data))


//no.12

//Delete a post
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE'
})
.then(res => res.json())
.then(data => console.log(data))

